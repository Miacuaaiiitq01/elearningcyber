import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import HomePages from "./pages/HomePages";
import Login from "./pages/Login";
import { AllUser } from "./pages/AllUser";
import { AddCourse } from "./pages/AddCourse";
import { MyCourse } from "./pages/MyCourse";
import { AccountSettings } from "./pages/Settings/AccountSettings";
import { Awaiting } from "./pages/Awaiting";
import { MyStudents } from "./pages/MyStudents";
import Spinner from "./components/Spinner/Spinner";

function App() {
  return (
    <div className="">
      {/* <Spinner /> */}
      <BrowserRouter>
        <Routes>
          <Route index element={<Login />} />
          <Route path="/homepage" element={<HomePages />} />
          <Route path="/all-user" element={<AllUser />} />
          <Route path="/add-course" element={<AddCourse />} />
          <Route path="/my-course" element={<MyCourse />} />
          <Route path="/account-settings" element={<AccountSettings />} />
          <Route path="/student-await" element={<Awaiting />} />
          <Route path="/my-students" element={<MyStudents />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
