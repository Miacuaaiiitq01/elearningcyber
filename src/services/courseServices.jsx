import { https } from "./config";
export const courseServices = {
  getCourses: (currentPage, pageSize) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang?page=${currentPage}&pageSize=${pageSize}&MaNhom=GP01`
    );
  },
  postCourse: (values) => {
    return https.post(`/api/QuanLyKhoaHoc/ThemKhoaHoc`, values);
  },
  getMyCourse: () => {
    return https.get(`/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01`);
  },
  deleteCourse: (maKhoaHoc) => {
    return https.delete(`/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${maKhoaHoc}`);
  },
  postMyStudents: (data) => {
    return https.post(`/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`, data);
  },
};
