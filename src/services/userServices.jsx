import { https } from "./config";
export const userServices = {
  postUserLogin: (data) => {
    return https.post(`/api/QuanLyNguoiDung/DangNhap`, data);
  },
  getUsers: (currentPage, pageSize) => {
    return https.get(
      `/api/QuanLyNguoiDung/LayDanhSachNguoiDung_PhanTrang?MaNhom=GP01&page=${currentPage}&pageSize=${pageSize}`
    );
  },
  postAddUsers: (data) => {
    return https.post(`/api/QuanLyNguoiDung/ThemNguoiDung`, data);
  },
  deleteUser: (data) => {
    return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${data}`);
  },
  putUser: (data) => {
    return https.put(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`, data);
  },
  postSubscribe: () => {
    return https.post(`/api/QuanLyKhoaHoc/DangKyKhoaHoc`);
  },
  postUnSubscribe: () => {
    return https.post(`/api/QuanLyKhoaHoc/HuyGhiDanh`);
  },
};
