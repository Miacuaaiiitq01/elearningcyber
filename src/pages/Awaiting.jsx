import React, { useEffect, useState } from "react";
import { courseServices } from "../services/courseServices";
import Nav from "./Nav";
import { DeleteOutlined } from "@ant-design/icons";

export const Awaiting = () => {
  const [maKhoaHoc, setMaKhoaHoc] = useState("abc12345");
  const [students, setStudents] = useState([]);
  console.log("maKhoaHoc: ", maKhoaHoc);
  useEffect(() => {
    courseServices
      .postMyStudents({ maKhoaHoc: maKhoaHoc })
      .then((res) => {
        console.log("res: ", res);
        setStudents(res.data);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }, []);
  // RENDER MY AWAIT STUDENT LIST
  const renderAwaitStudent = () => {
    return students?.map((item) => {
      return (
        <tr key={item?.taiKhoan} class="bg-white border-b ">
          <td class="px-6 py-2 font-medium">{item?.taiKhoan}</td>
          <td class="px-6 py-2 font-medium">{item?.biDanh}</td>
          <td class="px-6 py-2">{item?.hoTen}</td>
          <td class="px-6 py-2">
            <button className="pl-4">
              <DeleteOutlined className="text-red-500" />
            </button>
            <button>check</button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div className="flex">
      <div className="w-[340px] h-screen overflow-hidden shadow-md px-2 py-4 bg-[#f1f1f1]">
        <Nav />
      </div>
      <div className="w-full overflow-hidden px-5 pt-6 ">
        <div className="flex justify-between items-center">
          <div className="">
            <h1 className="pb-2 font-medium text-xl">
              Await <span class="text-base font-[400]">Student</span>
            </h1>
          </div>
        </div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table class="w-full text-sm text-left text-gray-500 ">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50  ">
              <tr>
                <th scope="col" class="px-6 py-3">
                  TaiKhoan
                </th>
                <th scope="col" class="px-6 py-3">
                  biDanh
                </th>
                <th scope="col" class="px-6 py-3">
                  hoTen
                </th>
                <th scope="col" class="px-6 py-3">
                  ACTION
                </th>
              </tr>
            </thead>
            <tbody>{renderAwaitStudent()}</tbody>
          </table>
        </div>
      </div>
    </div>
  );
};
