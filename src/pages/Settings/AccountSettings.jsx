import { Input, Form, message } from "antd";
import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import Nav from "../Nav";
import { userServices } from "../../services/userServices";
import { useSelector } from "react-redux";
import { userLocalStorage } from "../../services/localStoreServices";

export const AccountSettings = () => {
  const navigate = useNavigate();
  const admin = useSelector((state) => {
    return state.userSlice.userInfo;
  });
  useEffect(() => {
    if (!admin) {
      navigate("/");
    }
  }, []);
  const onFinish = (values) => {
    const newValues = {
      ...values,
      maLoaiNguoiDung: admin?.maLoaiNguoiDung,
      taiKhoan: admin?.taiKhoan,
    };
    userServices
      .putUser(newValues)
      .then((res) => {
        message.success("updated success");
        userLocalStorage.remove();
        setTimeout(() => {
          navigate("/");
        }, 500);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="flex">
      <div className="w-[340px] h-screen overflow-hidden shadow-md px-2 py-4 bg-[#f1f1f1]">
        <Nav />
      </div>
      <div className="w-full py-[100px]">
        <div className="font-bold w-full p-4 m-auto bg-[#f1f1f1] rounded-xl shadow-xl md:max-w-lg">
          <h1 className="text-center text-2xl font-[900]">Account Settings</h1>
          <Form
            className="mt-6"
            name="basic"
            labelCol={{
              span: 6,
            }}
            wrapperCol={{
              span: 18,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item label="UserName" className="mb-2" name="taiKhoan">
              <Input
                disabled
                placeholder={admin?.taiKhoan}
                className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md "
              />
            </Form.Item>
            <Form.Item
              label="Passwords"
              className="mb-2"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your Passwords!",
                },
              ]}
            >
              <Input className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md " />
            </Form.Item>
            <Form.Item
              label="Full Name"
              className="mb-2"
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Please input your Full Name!",
                },
              ]}
            >
              <Input className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md " />
            </Form.Item>
            <Form.Item
              label="PhoneNumber"
              className="mb-2"
              name="soDT"
              rules={[
                {
                  required: true,
                  message: "Please input your PhoneNumber!",
                },
              ]}
            >
              <Input className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md " />
            </Form.Item>
            <Form.Item
              label="Email"
              className="mb-2"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your Email!",
                },
              ]}
            >
              <Input className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md " />
            </Form.Item>
            <Form.Item
              label="Group"
              className="mb-2"
              name="maNhom"
              rules={[
                {
                  required: true,
                  message: "Please input your Group!",
                },
              ]}
            >
              <Input className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md " />
            </Form.Item>
            <div className="ant-form-item-control-input-content mt-5">
              <button
                type="submit"
                className="font-[500] w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-[#f64a6e] rounded-md hover:bg-[#f77259] focus:outline-none focus:bg-[#f77259]"
              >
                Update
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};
