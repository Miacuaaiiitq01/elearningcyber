import React, { useEffect } from "react";
import Nav from "./Nav";
import AllCourses from "./AllCourses";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

const HomePages = () => {
  const navigate = useNavigate();
  const admin = useSelector((state) => {
    return state.userSlice.userInfo;
  });
  useEffect(() => {
    if (!admin) {
      navigate("/");
    }
  }, []);
  return (
    <div className="flex">
      <div className="w-[340px] h-screen overflow-hidden shadow-md px-2 py-4 bg-[#f1f1f1]">
        <Nav />
      </div>
      <div className="w-full h-screen overflow-hidden">
        <AllCourses />
      </div>
    </div>
  );
};

export default HomePages;
