import React, { useEffect, useState } from "react";
import { courseServices } from "../services/courseServices";
import { EyeOutlined } from "@ant-design/icons";
import { Modal } from "antd";
import { Pagination } from "antd";
export default function AllCourses() {
  const [courses, setCourses] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizeItem, setSizeItem] = useState(10);
  const onChange = (pageNumber, pageSize) => {
    setCurrentPage(pageNumber);
    setSizeItem(pageSize);
  };
  const [listCourse, setListCourse] = useState([]);
  console.log("listCourse: ", listCourse);
  const [detailCourses, setDetailCourses] = useState({});
  console.log("detailCourses: ", detailCourses);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // GET COURSES
  useEffect(() => {
    courseServices
      .getCourses(currentPage, sizeItem)
      .then((res) => {
        setCourses(res.data.items);
        setListCourse(res.data);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }, [currentPage, sizeItem]);

  const renderCourses = () => {
    return courses.map((item) => {
      return (
        <tr class="bg-white border-b ">
          <td class="px-6 py-2 font-medium">{item?.maKhoaHoc}</td>
          <td class="px-6 py-2">{item?.tenKhoaHoc}</td>
          <td class="px-6 py-2">{item?.danhMucKhoaHoc?.tenDanhMucKhoaHoc}</td>
          <td class="px-6 py-2">{item?.nguoiTao?.hoTen}</td>
          <td class="px-6 py-2">
            <button
              className="pl-4"
              onClick={() => {
                showModal();
                setDetailCourses(item);
              }}
            >
              <EyeOutlined />
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <div className="px-5 py-12 ">
      <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <div className="text-center my-2">
          <p className="text-2xl font-bold uppercase">
            {detailCourses?.danhMucKhoaHoc?.tenDanhMucKhoaHoc}
          </p>
        </div>
        <div className="w-full h-80">
          <img
            className="object-cover h-full w-full"
            src={detailCourses?.hinhAnh}
            alt={detailCourses?.tenKhoaHoc}
          />
        </div>

        <div className="col-span-12 sm:col-span-8 p-2 sm:p-6">
          <p className="line-clamp-2 font-semibold md:leading-relaxed md:text-xl text-[#666666]">
            {detailCourses?.tenKhoaHoc}
          </p>
          <p className="mt-1 md:block hidden text-[#666666] font-[300]">
            {detailCourses?.moTa}
          </p>
          <p className="md:font-semibold font-light mt-1 text-[#666666]">
            {detailCourses?.nguoiTao?.hoTen}
          </p>
          <p className="sm:hidden line-clamp-2 font-semibold md:leading-relaxed md:text-xl text-[#666666] flex items-center">
            5.0
          </p>
          <div className="flex items-center justify-between">
            <div className="flex space-x-2 flex-wrap items-center text-sm pt-2 text-[#666666]">
              <p>13 hours</p>
              <p>·</p>
              <p>32 lectures</p>
            </div>
            <div className="text-lg font-semibold text-[#666666]">
              <p className="no-underline font-bold text-xl text-red-500">
                15,999,000<span className="text-sm ml-1">VNĐ</span>
              </p>
            </div>
          </div>
        </div>
      </Modal>
      <div className="flex">
        <h1 className="pb-4 font-medium text-xl">
          All <span class="text-base font-[400]">courses</span>
        </h1>
        <input
          type="text"
          className="font-[380] border-[2px] border-[#e8e8e8] rounded-md pr-24 pl-9 h-8 overflow-hidden focus:outline-none ml-80"
          placeholder="Search Course"
        />
      </div>
      <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-2">
        <table class="w-full text-sm text-left text-gray-500 ">
          <thead class="text-xs text-gray-700 uppercase bg-gray-50  ">
            <tr>
              <th scope="col" class="px-6 py-3">
                COURSE CODE
              </th>
              <th scope="col" class="px-6 py-3">
                COURSES
              </th>
              <th scope="col" class="px-6 py-3">
                CATEGORY
              </th>
              <th scope="col" class="px-6 py-3">
                LECTURERS
              </th>
              <th scope="col" class="px-6 py-3">
                ACTION
              </th>
            </tr>
          </thead>
          <tbody>{renderCourses()}</tbody>
        </table>
      </div>
      <div id="pagination_courses" className="text-center mt-6">
        <Pagination
          onChange={onChange}
          current={currentPage}
          defaultCurrent={1}
          total={listCourse?.totalCount}
        />
      </div>
    </div>
  );
}
