import React, { useEffect, useState } from "react";
import { courseServices } from "../services/courseServices";
import Nav from "./Nav";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { message } from "antd";

export const MyCourse = () => {
  const navigate = useNavigate();
  const admin = useSelector((state) => {
    return state.userSlice.userInfo;
  });
  useEffect(() => {
    if (!admin) {
      navigate("/");
    }
  }, []);
  const [allCourse, setAllCourse] = useState([]);
  const [myCourses, setMyCourses] = useState([]);

  useEffect(() => {
    courseServices
      .getMyCourse()
      .then((res) => {
        setAllCourse(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const filterMycourses = allCourse.filter((course) => {
    return course?.nguoiTao?.taiKhoan == admin?.taiKhoan;
  });
  const handleDelete = (maKhoaHoc) => {
    courseServices
      .deleteCourse(maKhoaHoc)
      .then((res) => {
        message.success("Deleted success");
        setTimeout(() => {
          window.location.reload();
        }, 300);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  let renderMyCourse = () => {
    return filterMycourses.map((item) => {
      return (
        <tr class="bg-white border-b ">
          <td class="px-6 py-2 font-medium">{item?.maKhoaHoc}</td>
          <td class="px-6 py-2">{item?.tenKhoaHoc}</td>
          <td class="px-6 py-2">{item?.danhMucKhoaHoc?.tenDanhMucKhoaHoc}</td>
          <td class="px-6 py-2">{item?.nguoiTao?.hoTen}</td>
          <td class="px-6 py-2">
            <button
              onClick={() => {
                handleDelete(item?.maKhoaHoc);
              }}
              className="pl-4"
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div className="flex">
      <div className="w-[340px] h-screen overflow-hidden shadow-md px-2 py-4 bg-[#f1f1f1]">
        <Nav />
      </div>
      <div className="w-full h-screen overflow-hidden px-5 py-12">
        <h1 className="pb-2 font-medium text-xl">
          {filterMycourses?.length} Courses
        </h1>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table class="w-full text-sm text-left text-gray-500 ">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50  ">
              <tr>
                <th scope="col" class="px-6 py-3">
                  COURSE CODE
                </th>
                <th scope="col" class="px-6 py-3">
                  COURSES
                </th>
                <th scope="col" class="px-6 py-3">
                  CATEGORY
                </th>
                <th scope="col" class="px-6 py-3">
                  LECTURERS
                </th>
                <th scope="col" class="px-6 py-3">
                  ACTION
                </th>
              </tr>
            </thead>
            <tbody>{renderMyCourse()}</tbody>
          </table>
        </div>
      </div>
    </div>
  );
};
