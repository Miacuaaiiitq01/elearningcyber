import React, { useEffect, useRef, useState } from "react";
import {
  WindowsOutlined,
  UsergroupAddOutlined,
  AppstoreAddOutlined,
  PlusOutlined,
  UserAddOutlined,
  Loading3QuartersOutlined,
  LogoutOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { motion } from "framer-motion";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { userLocalStorage } from "../services/localStoreServices";
import { message } from "antd";
import { useMediaQuery } from "react-responsive";
export default function Nav() {
  let isTabletMid = useMediaQuery({ query: "(max-width: 768px)" });
  const [open, setOpen] = useState(isTabletMid ? false : true);
  const sidebarRef = useRef();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const admin = useSelector((state) => {
    return state.userSlice.userInfo;
  });
  console.log("admin: ", admin);
  useEffect(() => {
    if (isTabletMid) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  }, [isTabletMid]);
  useEffect(() => {
    isTabletMid && setOpen(false);
  }, [pathname]);
  useEffect(() => {
    if (!admin) {
      navigate("/");
    }
  }, []);
  // LOGOUT admin
  const handleLogout = () => {
    userLocalStorage.remove();
    message.success("Logout successful");
    setTimeout(() => {
      window.location.reload();
    }, 500);
  };
  // const Nav_animation = isTabletMid;
  //   ? {
  //       open: {
  //         x: 0,
  //         width: "16rem",
  //         transition: {
  //           damping: 40,
  //         },
  //       },
  //       // closed: {
  //       //   x: -250,
  //       //   width: 0,
  //       //   transition: {
  //       //     damping: 40,
  //       //     delay: 0.15,
  //       //   },
  //       // },
  //     }
  //   : {
  //       open: {
  //         width: "16rem",
  //         transition: {
  //           damping: 40,
  //         },
  //       },
  //       // closed: {
  //       //   width: "4rem",
  //       //   transition: {
  //       //     damping: 40,
  //       //   },
  //       // },
  //     };
  return (
    <div>
      {/* <div
        onClick={() => setOpen(false)}
        className={`md:hidden fixed inset-0 max-h-screen z-[998] bg-black/50 ${
          open ? "block" : "hidden"
        } `}
      ></div>
      <motion.div
        ref={sidebarRef}
        variants={Nav_animation}
        initial={{ x: isTabletMid ? -250 : 0 }}
        animate={open ? "open" : "closed"}
        className="text-gray shadow-xl z-[999] max-w-[16rem]  w-[16rem] 
            overflow-hidden fixed
         h-screen "
      ></motion.div> */}
      <div className="logo pb-3 border-b border-slate-400">
        <div className="w-10 h-10 flex items-center ">
          <img src="/img/logo.png" alt="" />
          <p className="tracking-wider font-bold text-xl ">learning</p>
        </div>
      </div>
      <div className="navigation__bar ">
        <div className="overflow-scroll h-[550px] whitespace-pre px-2.5 text-[0.9rem] py-5 flex flex-col gap-1 font-medium overflow-x-hidden scrollbar-thin scrollbar-track-white scrollbar-thumb-slate-100 ">
          <div className="">
            <div className="border-b border-slate-400 pb-3">
              <small class="pl-3 pb-2 text-slate-500 inline-block">
                All Courses/ Users
              </small>
              <ul className="flex flex-col gap-2">
                <Link
                  to="/homepage"
                  className="cursor-pointer pl-3 flex items-center gap-6 p-2 rounded-lg tracking-wide hover:text-sky-500 hover:ring-sky-500 group-hover:text-white"
                >
                  <WindowsOutlined /> Courses
                </Link>
                <Link
                  to="/all-user"
                  className="pl-3 flex items-center gap-6 p-2 bg-slate-50 rounded-lg tracking-wide hover:text-sky-500 hover:ring-sky-500"
                >
                  <UsergroupAddOutlined /> Users
                </Link>
              </ul>
            </div>
            <div className="border-b border-slate-400 pt-2 pb-3">
              <small class="pl-3 pb-2 text-slate-500 inline-block">
                My Courses
              </small>
              <ul className="flex flex-col gap-2">
                <Link
                  to="/my-course"
                  className="cursor-pointer pl-3 flex items-center gap-6 p-2 bg-slate-50 rounded-lg tracking-wide hover:text-sky-500 hover:ring-sky-500"
                >
                  <AppstoreAddOutlined /> My courses
                </Link>
                <Link
                  to="/add-course"
                  className="pl-3 flex items-center gap-6 p-2 bg-slate-50 rounded-lg tracking-wide hover:text-sky-500 hover:ring-sky-500"
                >
                  <PlusOutlined /> Add course
                </Link>
              </ul>
            </div>
            <div className="border-b border-slate-400 pt-2 pb-3">
              <small class="pl-3 pb-2 text-slate-500 inline-block">
                My Student
              </small>
              <ul className="flex flex-col gap-2">
                <Link
                  to="/my-students"
                  className="pl-3 flex items-center gap-6 p-2 bg-slate-50 rounded-lg tracking-wide hover:text-sky-500 hover:ring-sky-500"
                >
                  <UserAddOutlined /> My students
                </Link>
                <Link
                  to="/student-await"
                  className="pl-3 flex items-center gap-6 p-2 bg-slate-50 rounded-lg tracking-wide hover:text-sky-500 hover:ring-sky-500"
                >
                  <Loading3QuartersOutlined /> Awaiting
                </Link>
              </ul>
            </div>
            <div className="border-b border-slate-400 pt-2 pb-3">
              <small class="pl-3 pb-2 text-slate-500 inline-block">
                Account
              </small>
              <ul className="flex flex-col gap-2">
                <Link
                  to="/account-settings"
                  className="pl-3 flex items-center gap-6 p-2 bg-slate-50 rounded-lg tracking-wide hover:text-sky-500 hover:ring-sky-500"
                >
                  <SettingOutlined /> Settings
                </Link>
              </ul>
            </div>
          </div>
        </div>
        <div class="flex-1 text-sm z-50  max-h-48 my-auto  whitespace-pre   w-full  font-medium  ">
          <div class="flex p-3 items-center justify-between">
            <ul>
              <li>
                <button
                  onClick={() => {
                    handleLogout();
                  }}
                  class="link text-red-500 flex items-center"
                >
                  <LogoutOutlined className="mr-3" />
                  {admin?.hoTen}
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
