import React, { useEffect, useState } from "react";

import { Modal, Button, Checkbox, Form, Input, message } from "antd";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { courseServices } from "../services/courseServices";
import Nav from "./Nav";
export const AddCourse = () => {
  const admin = useSelector((state) => {
    return state.userSlice.userInfo;
  });
  const navigate = useNavigate();
  useEffect(() => {
    if (!admin) {
      navigate("/");
    }
  }, []);
  const [imgSrc, setImgSrc] = useState();
  const [file, setFile] = useState("");
  const onFinish = (values) => {
    var utc = new Date().toJSON().slice(0, 10).replace(/-/g, "/");
    const newValues = {
      ...values,
      hinhAnh: file?.name,
      taiKhoanNguoiTao: admin?.taiKhoan,
      ngayTao: utc,
    };
    courseServices
      .postCourse(newValues)
      .then((res) => {
        message.success("Add course successful");
      })
      .catch((err) => {
        message.warning(err.response.data);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const handleChangFile = (e) => {
    let file = e.target.files[0];
    setFile(file);
    console.log("file: ", file);
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/gif" ||
      file.type === "image/png"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        console.log(e.target.result);
        setImgSrc(e.target.result); // hình base 64
      };
    }
  };
  return (
    <div className="flex overflow-y-hidden font-medium overflow-scroll h-full whitespace-pre scrollbar-thin scrollbar-track-white scrollbar-thumb-slate-300">
      <div className="w-[340px] h-screen overflow-hidden shadow-md px-2 py-4 bg-[#f1f1f1]">
        <Nav />
      </div>
      <div className="w-full h-screen overflow-hidden">
        <Form
          className="mt-6"
          name="basic"
          labelCol={{
            span: 6,
          }}
          wrapperCol={{
            span: 18,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="CourseCode"
            className="mb-2"
            name="maKhoaHoc"
            rules={[
              {
                required: true,
                message: "Please input your CourseCode!",
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md " />
          </Form.Item>
          <Form.Item
            label="Alias"
            className="mb-2"
            name="biDanh"
            rules={[
              {
                required: true,
                message: "Please input your Alias!",
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
          </Form.Item>
          <Form.Item
            label="Course Name"
            className="mb-2"
            name="tenKhoaHoc"
            rules={[
              {
                required: true,
                message: "Please input your CourseName!",
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
          </Form.Item>
          <Form.Item
            label="Decription"
            className="mb-2"
            name="moTa"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
          </Form.Item>
          <Form.Item
            label="Views"
            className="mb-2"
            name="luotXem"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
          </Form.Item>
          <Form.Item
            label="
            Evaluate"
            className="mb-2"
            name="danhGia"
            rules={[
              {
                required: true,
                message: "Please input your Evaluate!",
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
          </Form.Item>
          <Form.Item label="Picture" className="mb-2" name="hinhAnh">
            <Input
              onChange={handleChangFile}
              type="file"
              className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
            <img
              style={{ width: 100, height: 100 }}
              src={imgSrc}
              alt=""
              accept="image/png, image/jpg, image/jpeg, image/gif"
            />
          </Form.Item>
          <Form.Item
            label="GroupCode"
            className="mb-2"
            name="maNhom"
            rules={[
              {
                required: true,
                message: "Please input your GroupCode!",
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
          </Form.Item>
          <Form.Item
            label="Categories"
            className="mb-2"
            name="maDanhMucKhoaHoc"
            rules={[
              {
                required: true,
                message: "Please input your Categories",
              },
            ]}
          >
            <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
          </Form.Item>
          <Form.Item className="mt-3">
            <button
              type="submit"
              className="font-[500] w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-[#f64a6e] rounded-md hover:bg-[#f77259] focus:outline-none focus:bg-[#f77259]"
            >
              Add
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
