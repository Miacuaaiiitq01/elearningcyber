import React, { useEffect, useState } from "react";
import { userServices } from "../services/userServices";
import { DeleteOutlined } from "@ant-design/icons";
import {
  Modal,
  Button,
  Checkbox,
  Form,
  Input,
  message,
  Pagination,
} from "antd";

import Nav from "./Nav";

export const AllUser = () => {
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizeItem, setSizeItem] = useState(10);
  const onChange = (pageNumber, pageSize) => {
    setCurrentPage(pageNumber);
    setSizeItem(pageSize);
  };
  const [listUsers, setListUsers] = useState([]);
  console.log("listUsers: ", listUsers);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  useEffect(() => {
    userServices
      .getUsers(currentPage, sizeItem)
      .then((res) => {
        setUsers(res.data.items);
        setListUsers(res.data);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }, [currentPage, sizeItem]);
  const onFinish = (values) => {
    console.log("values: ", values);
    userServices
      .postAddUsers(values)
      .then((res) => {
        message.success("Add user successful");
        window.location.reload();
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const handleDelete = (userName) => {
    userServices
      .deleteUser(userName)
      .then((res) => {
        message.success(res.data);
        setTimeout(() => {
          window.location.reload();
        }, 500);
      })
      .catch((err) => {
        message.warning(err.response);
      });
  };
  const renderUsers = () => {
    return users.map((item) => {
      return (
        <tr key={item?.taiKhoan} class="bg-white border-b ">
          <td class="px-6 py-2 font-medium">{item?.taiKhoan}</td>
          <td class="px-6 py-2">{item?.hoTen}</td>
          <td class="px-6 py-2">{item?.email}</td>
          <td class="px-6 py-2">{item?.soDT}</td>
          <td class="px-6 py-2">
            <button
              className="pl-4"
              onClick={() => {
                handleDelete(item?.taiKhoan);
              }}
            >
              <DeleteOutlined className="text-red-500" />
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <>
      <div className="flex">
        <div className="w-[340px] h-screen overflow-hidden shadow-md px-2 py-4 bg-[#f1f1f1]">
          <Nav />
        </div>
        <div className="w-full overflow-hidden px-5 pt-6 ">
          <div className="flex justify-between items-center">
            <div className="">
              <h1 className="pb-2 font-medium text-xl">
                All <span class="text-base font-[400]">User</span>
              </h1>
              <button
                className="mb-4 bg-gradient-to-tl from-[#fcd34d] to-[#ef4444] text-white text-sm hover:text-base transition-all duration-500 py-1 px-4 rounded-md"
                onClick={() => {
                  showModal();
                }}
              >
                Add user
              </button>
            </div>
            <input
              type="text"
              className="font-[380] border-[2px] border-[#e8e8e8] rounded-md pr-24 pl-9 h-8 overflow-hidden  focus:outline-none"
              placeholder="Search User"
            />
          </div>
          <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
            {/* .............. */}
            <Form
              className="mt-6"
              name="basic"
              labelCol={{
                span: 6,
              }}
              wrapperCol={{
                span: 18,
              }}
              style={{
                maxWidth: 600,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Account"
                className="mb-2"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Please input your Account!",
                  },
                ]}
              >
                <Input className="w-full px-4 py-2 text-gray-900 bg-white border rounded-md " />
              </Form.Item>
              <Form.Item
                label="Password"
                className="mb-2"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
              </Form.Item>
              <Form.Item
                label="Full name"
                className="mb-2"
                name="hoTen"
                rules={[
                  {
                    required: true,
                    message: "Please input your Full Name!",
                  },
                ]}
              >
                <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
              </Form.Item>
              <Form.Item
                label="Phone number"
                className="mb-2"
                name="soDT"
                rules={[
                  {
                    required: true,
                    message: "Please input your Phone number!",
                  },
                ]}
              >
                <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
              </Form.Item>
              <Form.Item
                label="Usercode"
                className="mb-2"
                name="maLoaiNguoiDung"
                rules={[
                  {
                    required: true,
                    message: "Please input your Usercode!",
                  },
                ]}
              >
                <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
              </Form.Item>
              <Form.Item
                label="GroupCode"
                className="mb-2"
                name="maNhom"
                rules={[
                  {
                    required: true,
                    message: "Please input your GroupCode!",
                  },
                ]}
              >
                <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
              </Form.Item>
              <Form.Item
                label="Email"
                className="mb-2"
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Please input your Email!",
                  },
                ]}
              >
                <Input className="w-full px-4 py-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40" />
              </Form.Item>

              <Form.Item className="mt-3">
                <button
                  type="submit"
                  className="font-[500] w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-[#f64a6e] rounded-md hover:bg-[#f77259] focus:outline-none focus:bg-[#f77259]"
                >
                  Add
                </button>
              </Form.Item>
            </Form>
          </Modal>
          <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left text-gray-500 ">
              <thead class="text-xs text-gray-700 uppercase bg-gray-50  ">
                <tr>
                  <th scope="col" class="px-6 py-3">
                    USERNAME
                  </th>
                  <th scope="col" class="px-6 py-3">
                    FULLNAME
                  </th>
                  <th scope="col" class="px-6 py-3">
                    EMAIL
                  </th>
                  <th scope="col" class="px-6 py-3">
                    PHONE NUMBER
                  </th>
                  <th scope="col" class="px-6 py-3">
                    ACTION
                  </th>
                </tr>
              </thead>
              <tbody>{renderUsers()}</tbody>
            </table>
          </div>
          <div id="pagination_courses" className="text-center mt-4">
            <Pagination
              onChange={onChange}
              current={currentPage}
              defaultCurrent={1}
              total={listUsers?.totalCount}
            />
          </div>
        </div>
      </div>
    </>
  );
};
